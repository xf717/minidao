package com.minidao.annotation;

import java.lang.annotation.*;  

/** 
 * daoInterface的方法参数的注解
 */  
@Target(ElementType.PARAMETER)  
@Retention(RetentionPolicy.RUNTIME)  
@Documented  
public @interface Param {  
    String value();  
}  
